Source: xhtmlrenderer
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Build-Depends: ant,
               debhelper (>= 10~),
               default-jdk,
               javahelper
Build-Depends-Indep: libitext-java (>= 2.0)
Standards-Version: 3.9.8
Homepage: https://code.google.com/p/flying-saucer/
Vcs-Git: https://anonscm.debian.org/git/pkg-java/xhtmlrenderer.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/xhtmlrenderer.git

Package: libcore-renderer-java
Architecture: all
Depends: libitext-java (>= 2.0),
         ${misc:Depends}
Description: Java library that provides an XML/XHTML/CSS 2.1 Renderer
 xhtmlrender is an XML/CSS renderer, which means it takes
 XML files as input, applies formatting and styling using
 CSS, and generates a rendered representation of that XML as
 output. The output may go to the screen (in a GUI), to an
 image or to a PDF file.
 .
 The main target for content is XHTML 1.0 (strict), an XML
 document format that standardizes HTML. However,
 xhtmlrenderer accepts any well-formed XML for rendering as
 long as CSS is provided that tells how to lay it out.
 .
 In the case of XHTML, default stylesheets are provided out
 of the box and packaged within the library.

Package: libcore-renderer-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: libcore-renderer-java
Suggests: default-jdk-doc
Description: Documentation for libcore-renderer-java
 Documentation for xhtmlrender that is an XML/CSS renderer,
 which means it takes XML files as input, applies formatting
 and styling using CSS, and generates a rendered representation
 of that XML as output. The output may go to the screen (in a
 GUI), to an image or to a PDF file.
 .
 The main target for content is XHTML 1.0 (strict), an XML
 document format that standardizes HTML. However,
 xhtmlrenderer accepts any well-formed XML for rendering as
 long as CSS is provided that tells how to lay it out.
 .
 In the case of XHTML, default stylesheets are provided out
 of the box and packaged within the library.
 .
 This package contains the javadoc files of
 libcore-renderer-java and the user guide reference for
 xhtmlrenderer.
